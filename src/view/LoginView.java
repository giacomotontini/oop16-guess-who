package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Optional;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controller.Controller;
import controller.ControllerImpl;
import utilities.Audio;
import utilities.ResourceLoader;
import utilities.ScreenResolution;
import utilities.Audio.Song;

/* THINGS TO ASK DENDO:
 *      PERCHE QUANDO CHIUDO MESSAGEDIALOG (COMPARE QUANDO NON METTO ID O PSW) SI CHIUDE FRAME
 *      PERCHE SIA NELLA MESSAGE SIA NELLA MAIN WINDOW I BOTTONI SONO TROPPO IN BASSO
 *      COME FACCIO A METTERE LOGIN E REGISTER AFFIANCATI E NON UNO SOTTO ALL'ALTRO
 */
/**
 * 
 * Login view.
 *
 */
public class LoginView {
    private static final String TITLE = "Guess Who?";
    private static final int VGAP = 30;
    private static final int VGAPL = 20;
    private static final double FRAMEWIDTH_PROP = 0.55;
    private static final double FRAMEHEIGHT_PROP = 0.55;
    private static final double LOGOHEIGHT_PROP = 0.35;
    private static final int R = 208;
    private static final int G = 72;
    private static final int B = 72;

    /**
     * Creates a new login view where users can login or add characters.
     */
    public LoginView() {
        final Controller controller = ControllerImpl.getController();
        final GUIFactory factory = GUIFactoryImpl.getFactory();
        final MyFrame frame = new MyFrame(TITLE, Optional.empty(), new FlowLayout());
        final JPanel contentPane = factory.createPanel(Optional.empty());
        final JPanel buttonPane = factory.createPanel(Optional.empty());
        final JPanel utilityPane = factory.createPanel(Optional.empty());
        final MyLabel logo = new MyLabel(new ImageIcon(ResourceLoader.loadImage("/images/logo.png", true)));
        final JLabel jlbl = factory.createLabel("Username:");
        final JLabel jlb2 = factory.createLabel("Password:");
        final JTextField idText = new JTextField();
        idText.setFocusable(true);
        idText.setHorizontalAlignment(JTextField.CENTER);
        final JPasswordField pswText = new JPasswordField();
        pswText.setHorizontalAlignment(JTextField.CENTER);
        final JButton btnLogin = factory.createButton("Login");
        final JButton btnRegister = factory.createButton("Registrati");
        final JButton btnAddChar = factory.createButton("Aggiungi personaggio");
        logo.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnLogin.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnRegister.setAlignmentX(Component.CENTER_ALIGNMENT);
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
        utilityPane.setLayout(new BoxLayout(utilityPane, BoxLayout.X_AXIS));
        btnLogin.addActionListener(e -> {
            Audio.playSound(Song.BUTTON_CLICK, false);
            final String id = idText.getText();
            final char[] psw = pswText.getPassword();
            if (id.trim().equals("") || psw.length == 0) {
                MyDialog.showMessageDialog(frame, "Devi inserire sia username che password!");
            } else {
                if (controller.checkLogin(id, String.copyValueOf(psw))) {
                    MyDialog.showMessageDialog(frame, "Benvenuto " + id + "!");
                    controller.startGame();
                    frame.dispose();
                } else {
                    MyDialog.showMessageDialog(frame, "Credenziali errate!");
                }
            }
        });
        btnAddChar.addActionListener(e -> {
            controller.callCharView();
        });
        btnRegister.addActionListener(e -> {
            Audio.playSound(Song.BUTTON_CLICK, false);
            final String id = idText.getText();
            final char[] psw = pswText.getPassword();
            if (id.trim().equals("") || psw.length == 0) {
                MyDialog.showMessageDialog(frame, "Devi inserire sia username che password!");
            } else {
                if (controller.registerUser(id, psw)) {
                    MyDialog.showMessageDialog(frame, "Utente creato!");
                    pswText.setText("");
                } else {
                    MyDialog.showMessageDialog(frame, "ID gi� in uso!");
                }
            }
        });
        contentPane.add(logo);
        contentPane.add(Box.createRigidArea(new Dimension(0, VGAP)));
        contentPane.add(jlbl);
        contentPane.add(idText);
        contentPane.add(Box.createRigidArea(new Dimension(0, VGAP)));
        contentPane.add(jlb2);
        contentPane.add(pswText);
        contentPane.add(Box.createRigidArea(new Dimension(0, VGAP)));
        buttonPane.add(btnLogin);
        buttonPane.add(Box.createRigidArea(new Dimension(VGAP, 0)));
        buttonPane.add(btnRegister);
        contentPane.add(buttonPane);
        utilityPane.add(btnAddChar);
        contentPane.add(Box.createRigidArea(new Dimension(0, VGAP - VGAPL)));
        contentPane.add(utilityPane);
        contentPane.add(Box.createRigidArea(new Dimension(0, VGAP)));
        frame.setPreferredSize(new Dimension((int) (ScreenResolution.getScreenWidth() * FRAMEWIDTH_PROP),
                (int) (ScreenResolution.getScreenHeight() * FRAMEHEIGHT_PROP)));
        logo.setPreferredSize(new Dimension((int) frame.getPreferredSize().getWidth(),
                (int) (frame.getPreferredSize().getHeight() * LOGOHEIGHT_PROP)));
        frame.getMainPanel().add(contentPane);
        frame.getMainPanel().setBackground(new Color(R, G, B));
        frame.pack();
        frame.setLocation((ScreenResolution.getScreenWidth() - frame.getWidth()) / 2,
                (ScreenResolution.getScreenHeight() - frame.getHeight()) / 2);
        frame.setVisible(true);
    }
}
