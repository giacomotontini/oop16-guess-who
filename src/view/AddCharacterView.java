package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Controller;
import controller.ControllerImpl;
import utilities.ResourceLoader;
import utilities.ScreenResolution;

/**
 * View that allows users to create new characters.
 *
 */
public class AddCharacterView {

    private static final String TITLE = "Guess Who?";
    private static final int GAP = 30;
    private static final double FRAMEWIDTH_PROP = 0.55;
    private static final double FRAMEHEIGHT_PROP = 0.55;
    private static final double LOGOHEIGHT_PROP = 0.35;
    private static final int R = 208;
    private static final int G = 72;
    private static final int B = 72;
    private static final int ROWS = 16;
    private static final int COLS = 2;

    private final GUIFactory factory;
    private final JPanel contentPane;
    private final JPanel midPane;
    private final JPanel leftGridPane;
    private final JPanel rightGridPane;
    private final JPanel upperPane;
    private final JPanel lowerPane;
    private final MyLabel logo;
    private final JLabel welcomeLabel;
    private final List<String> questionLabel;
    private final List<JComponent> questionInputs;
    private final GridLayout grid;
    private final JButton chooseImage;
    private final JButton create;
    private final Controller controller;
    private final MyFrame frame;
    private final JTextField name;
    private final JComboBox<String> sex;
    private final JComboBox<String> eyeColor;
    private final JComboBox<String> hairType;
    private final JComboBox<String> skinColor;
    private final JCheckBox hasBeard;
    private final JComboBox<String> beardColor;
    private final JCheckBox hasMust;
    private final JCheckBox hasHat;
    private final JCheckBox hasEar;
    private final JCheckBox hasGlass;
    private final JComboBox<String> hairColor;
    private final JComboBox<String> dressType;
    private final JComboBox<String> mustColor;
    private final JComboBox<String> packages;

    private String path = "";

    /**
     * creates a new CHaracter View.
     */
    public AddCharacterView() {
        this.controller = ControllerImpl.getController();
        factory = GUIFactoryImpl.getFactory();
        questionInputs = new ArrayList<JComponent>();
        this.frame = new MyFrame(TITLE, Optional.empty(), new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        grid = new GridLayout(0, COLS);
        contentPane = factory.createPanel(Optional.empty());
        midPane = factory.createPanel(Optional.empty());
        leftGridPane = factory.createPanel(Optional.of(grid));
        rightGridPane = factory.createPanel(Optional.of(grid));
        upperPane = factory.createPanel(Optional.empty());
        lowerPane = factory.createPanel(Optional.empty());

        welcomeLabel = factory.createLabel(
                "<html>Seleziona i valori e crea il personaggio!<br/>I campi contrassegnati con * sono obbligatori!</html>");
        welcomeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        logo = new MyLabel(new ImageIcon(ResourceLoader.loadImage("/images/logo.png", true)));
        questionLabel = Arrays.asList("Nome *", "Sesso *", "Colore pelle *", "Colore occhi *", "Tipo capelli *",
                "Colore capelli", "Barba", "Colore barba", "Baffi", "Colore baffi", "Cappello", "Orecchini", "Occhiali",
                "Abito *", "Pacco personaggi *", "Immagine *");

        this.name = new JTextField();
        questionInputs.add(name);
        sex = factory.createComboBox(Arrays.asList("UOMO", "DONNA"));
        this.sex.setSelectedIndex(-1);
        questionInputs.add(sex);
        this.skinColor = factory.createComboBox(this.controller.getColors());
        this.skinColor.setSelectedIndex(-1);
        questionInputs.add(skinColor);
        this.eyeColor = factory.createComboBox(this.controller.getColors());
        this.eyeColor.setSelectedIndex(-1);
        questionInputs.add(eyeColor);
        this.hairType = factory.createComboBox(controller.getHairTypes());
        this.hairType.setSelectedIndex(-1);
        questionInputs.add(hairType);
        this.hairColor = factory.createComboBox(this.controller.getColors());
        this.hairColor.setSelectedIndex(-1);
        this.hairColor.setEnabled(false);
        questionInputs.add(hairColor);
        this.hasBeard = new JCheckBox();
        questionInputs.add(hasBeard);
        this.beardColor = factory.createComboBox(this.controller.getColors());
        this.beardColor.setSelectedIndex(-1);
        this.beardColor.setEnabled(false);
        questionInputs.add(beardColor);
        this.hasMust = new JCheckBox();
        questionInputs.add(hasMust);
        this.mustColor = factory.createComboBox(this.controller.getColors());
        this.mustColor.setSelectedIndex(-1);
        this.mustColor.setEnabled(false);
        questionInputs.add(mustColor);
        this.hasHat = new JCheckBox();
        questionInputs.add(hasHat);
        this.hasEar = new JCheckBox();
        questionInputs.add(hasEar);
        this.hasGlass = new JCheckBox();
        questionInputs.add(hasGlass);
        this.dressType = factory.createComboBox(this.controller.getDressTypes());
        this.dressType.setSelectedIndex(-1);
        questionInputs.add(dressType);
        this.packages = factory.createComboBox(this.controller.getPackList());
        this.packages.setSelectedIndex(-1);
        questionInputs.add(packages);
        chooseImage = new JButton("Scegli...");
        questionInputs.add(chooseImage);
        create = factory.createButton("Crea");
        create.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseImage.addActionListener(e -> {
            final JFileChooser fc = new JFileChooser();
            if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                final File f = fc.getSelectedFile();
                if (!fc.getTypeDescription(f).equals("Immagine PNG")
                        && !fc.getTypeDescription(f).equals("Immagine JPEG")) {
                    MyDialog.showMessageDialog(frame, "L'immagine deve essere in formato .jpeg o .png!");
                } else {
                    path = f.getAbsolutePath();
                }
            }
        });
        this.hasBeard.addActionListener(e -> {
            beardColor.setEnabled(!beardColor.isEnabled());
        });
        this.hairType.addActionListener(e -> {
            if (hairType.getSelectedItem().toString().equals("CALVO")) {
                hairColor.setEnabled(false);
            } else {
                hairColor.setEnabled(true);
            }
        });
        this.hasMust.addActionListener(e -> {
            mustColor.setEnabled(!mustColor.isEnabled());
        });
        create.addActionListener(e -> {
            if (!checkInputs()) {
                MyDialog.showMessageDialog(frame,
                        "Devi selezionare tutti i valori obbligatori per creare il personaggio!");
            } else {
                String beardCol = "";
                String hairCol = "";
                String mustCol = "";
                final boolean ben = this.beardColor.isEnabled();
                final boolean hen = this.hairColor.isEnabled();
                final boolean men = this.mustColor.isEnabled();
                if (ben && hen && !men) {
                    beardCol = beardColor.getSelectedItem().toString();
                    hairCol = hairColor.getSelectedItem().toString();
                }
                if (ben && !hen && men) {
                    beardCol = beardColor.getSelectedItem().toString();
                    mustCol = mustColor.getSelectedItem().toString();
                }
                if (!ben && hen && men) {
                    hairCol = hairColor.getSelectedItem().toString();
                    mustCol = mustColor.getSelectedItem().toString();
                }
                if (ben && hen && men) {
                    beardCol = beardColor.getSelectedItem().toString();
                    hairCol = hairColor.getSelectedItem().toString();
                    mustCol = mustColor.getSelectedItem().toString();
                }
                controller.createCharacter(name.getText(), sex.getSelectedItem().toString(),
                        eyeColor.getSelectedItem().toString(), skinColor.getSelectedItem().toString(), hairCol,
                        hasBeard.isSelected(), hasMust.isSelected(), beardCol, mustCol, hasHat.isSelected(),
                        hasEar.isSelected(), hasGlass.isSelected(), hairType.getSelectedItem().toString(),
                        dressType.getSelectedItem().toString(), packages.getSelectedItem().toString(), path);
                MyDialog.showMessageDialog(frame, "Personaggio creato con successo!");
                frame.dispose();
            }
        });
        for (int j = 0; j < ROWS; j++) {
            if (j < ROWS / 2) {
                leftGridPane.add(factory.createLabel(questionLabel.get(j)));
                leftGridPane.add(questionInputs.get(j));
            } else {
                rightGridPane.add(factory.createLabel(questionLabel.get(j)));
                rightGridPane.add(questionInputs.get(j));
            }
        }

        frame.setPreferredSize(new Dimension((int) (ScreenResolution.getScreenWidth() * FRAMEWIDTH_PROP),
                (int) (ScreenResolution.getScreenHeight() * FRAMEHEIGHT_PROP)));
        logo.setPreferredSize(new Dimension((int) frame.getPreferredSize().getWidth(),
                (int) (frame.getPreferredSize().getHeight() * LOGOHEIGHT_PROP)));

        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        midPane.setLayout(new BoxLayout(midPane, BoxLayout.X_AXIS));

        upperPane.add(welcomeLabel);
        midPane.add(leftGridPane);
        midPane.add(Box.createRigidArea(new Dimension(GAP, 0)));
        midPane.add(rightGridPane);
        lowerPane.add(create);
        contentPane.add(upperPane);
        contentPane.add(Box.createRigidArea(new Dimension(0, GAP)));
        contentPane.add(midPane);
        contentPane.add(Box.createRigidArea(new Dimension(0, GAP)));
        contentPane.add(lowerPane);

        frame.getMainPanel().add(contentPane);
        frame.getMainPanel().setBackground(new Color(R, G, B));
        frame.pack();
        frame.setLocation((ScreenResolution.getScreenWidth() - frame.getWidth()) / 2,
                (ScreenResolution.getScreenHeight() - frame.getHeight()) / 2);
        frame.setVisible(true);
    }

    /**
     * Shows an error message to user.
     * 
     * @param msg
     *            the message to be shown.
     */
    public void showMessage(final String msg) {
        MyDialog.showMessageDialog(frame, msg);
    }

    private boolean checkInputs() {
        return (name.getText().trim().equals("") || sex.getSelectedItem() == null || hairType.getSelectedItem() == null
                || dressType.getSelectedItem() == null || skinColor.getSelectedItem() == null
                || eyeColor.getSelectedItem() == null || packages.getSelectedItem() == null || path.equals("")) ? false
                        : true;
    }

}
