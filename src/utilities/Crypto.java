package utilities;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * 
 * 
 *
 */
public final class Crypto {
    private static final Crypto SINGLETON = new Crypto();
    /**
     * Each token produced by this class uses this identifier as a prefix.
     */
    public static final String ID = "$31$";

    /**
     * The minimum recommended cost, used by default.
     */
    public static final int DEFAULT_COST = 16;

    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";

    private static final int SIZE = 128;
    private static final int MAX_COST = 30;

    private static final Pattern LAYOUT = Pattern.compile("\\$31\\$(\\d\\d?)\\$(.{43})");

    private final SecureRandom random;

    private final int cost;

    private Crypto() {
        this(DEFAULT_COST);
    }

    /**
     * 
     * @param cost
     *            a.
     */
    public Crypto(final int cost) {
        iterations(cost); /* Validate cost */
        this.cost = cost;
        this.random = new SecureRandom();
    }

    /**
     * 
     * @return s.
     */
    public static Crypto getCrypto() {
        return SINGLETON;
    }

    /**
     * Create a password manager with a specified cost
     * 
     * @param cost
     *            the exponential computational cost of hashing a password, 0 to 30
     */

    private static int iterations(final int cost) {
        if ((cost < 0) || (cost > MAX_COST)) {
            throw new IllegalArgumentException("cost: " + cost);
        }
        return 1 << cost;
    }

    /**
     * Hash a password for storage.
     * 
     * @param password
     *            c.
     * @return a secure authentication token to be stored for later authentication
     */
    public String hash(final char... password) {
        final byte[] salt = new byte[SIZE / 8];
        random.nextBytes(salt);
        final byte[] dk = pbkdf2(password, salt, 1 << cost);
        final byte[] hash = new byte[salt.length + dk.length];
        System.arraycopy(salt, 0, hash, 0, salt.length);
        System.arraycopy(dk, 0, hash, salt.length, dk.length);
        final Base64.Encoder enc = Base64.getUrlEncoder().withoutPadding();
        return ID + cost + '$' + enc.encodeToString(hash);
    }

    /**
     * 
     * @param password
     *            a.
     * @param token
     *            a
     * @return a
     */
    public boolean authenticate(final char[] password, final String token) {
        final Matcher m = LAYOUT.matcher(token);
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid token format");
        }
        final int iterations = iterations(Integer.parseInt(m.group(1)));
        final byte[] hash = Base64.getUrlDecoder().decode(m.group(2));
        final byte[] salt = Arrays.copyOfRange(hash, 0, SIZE / 8);
        final byte[] check = pbkdf2(password, salt, iterations);
        int zero = 0;
        for (int idx = 0; idx < check.length; ++idx) {
            zero |= hash[salt.length + idx] ^ check[idx];
        }
        return zero == 0;
    }

    private static byte[] pbkdf2(final char[] password, final byte[] salt, final int iterations) {
        final KeySpec spec = new PBEKeySpec(password, salt, iterations, SIZE);
        try {
            final SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
            return f.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException("Missing algorithm: " + ALGORITHM, ex);
        } catch (InvalidKeySpecException ex) {
            throw new IllegalStateException("Invalid SecretKeyFactory", ex);
        }
    }
}
