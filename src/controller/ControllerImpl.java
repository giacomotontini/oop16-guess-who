package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.swing.ImageIcon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Model;
import model.ModelImpl;
import model.Question;
import model.exception.AttemptException;
import model.exception.NoMoreQuestionAvailableException;
import model.exception.PendingAIGuessingCharacter;
import model.exception.UserLiedException;
import utilities.CharacterDeserializer;
import utilities.Crypto;
import utilities.Logger;
import utilities.ResourceLoader;
import view.AddCharacterView;
import view.GameView;
import view.GameViewImpl;
import view.LoginView;
import view.MenuView;
import model.Character;
import model.Character.CharacterBuilder;
import model.Character.Colors;
import model.Character.Dress;
import model.Character.Hair;

/**
 * This class manages the game and interactions between model and view.
 *
 */
public final class ControllerImpl implements Controller {
    private static final String FS = System.getProperty("file.separator");
    private static final String UH = System.getProperty("user.home");
    private static final String GAMEDIR = "guesswho";
    private static final String GAME_PATH = UH + FS + GAMEDIR;
    private static final String USER_DIR_PATH = UH + FS + GAMEDIR + FS + "users";
    private static final String USER_JSON_PATH = UH + FS + GAMEDIR + FS + "json";
    private static final String USER_IMAGES_PATH = UH + FS + GAMEDIR + FS + "images";
    private static final String USER_FILE_PATH = UH + FS + GAMEDIR + FS + "users" + FS + "users.txt";
    private static final String JSON_EXT = ".json";
    private static final String USER_IMG = "/images";
    private static final String IMG_PACK_1 = "pack1";
    private static final String IMG_PACK_2 = "pack2";
    private static final String JSONS_PATH = "/json";
    private static final String SPLITUSER = "|";
    private static final String SPLITCRED = "\\\\";
    private static final String SEX = "UOMO";
    private static final int JSON_EXT_LENGTH = 5;
    private final Crypto myCrypto = Crypto.getCrypto();
    private static final Logger LOG = Logger.getLogger();
    private static final Controller SINGLETON = new ControllerImpl();
    private Character newChar;
    private Model myModel;
    private GameView myView;
    private Gson gson;
    private GsonBuilder gsonBuilder;

    private ControllerImpl() {
        this.gsonBuilder = new GsonBuilder();
        if (!checkAndCreate(GAME_PATH)) {
            checkAndCreate(USER_DIR_PATH);
            checkAndCreate(USER_JSON_PATH);
            checkAndCreate(USER_IMAGES_PATH);
            if (!checkAndCreate(USER_IMAGES_PATH + FS + IMG_PACK_1)) {
                fixCharactersPic(USER_IMG, "/pack1");
            }
            if (!checkAndCreate(USER_IMAGES_PATH + FS + IMG_PACK_2)) {
                fixCharactersPic(USER_IMG, "/pack2");
            }
            if (!checkFile(USER_FILE_PATH)) {
                try {
                    Files.createFile(Paths.get(USER_FILE_PATH));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            checkAndCreate(USER_DIR_PATH);
            checkAndCreate(USER_JSON_PATH);
            checkAndCreate(USER_IMAGES_PATH);
            if (!checkAndCreate(USER_IMAGES_PATH + FS + IMG_PACK_1)) {
                fixCharactersPic("/images", "/pack1");
            }
            if (!checkAndCreate(USER_IMAGES_PATH + FS + IMG_PACK_2)) {
                fixCharactersPic("/images", "/pack2");
            }
            if (!checkFile(USER_FILE_PATH)) {
                try {
                    Files.createFile(Paths.get(USER_FILE_PATH));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 
     * @return an istance of controller
     */
    public static Controller getController() {
        return ControllerImpl.SINGLETON;
    }

    @Override
    public Map<Character, Boolean> getHumanCharactersLeft() {
        return this.myModel.getHumanCharacters();
    }

    @Override
    public Map<Character, Boolean> getAICharactersLeft() {
        return this.myModel.getAICharacters();
    }

    @Override
    public Set<Question> getAvailableQuestions() {
        return this.myModel.getHumanQuestions();
    }

    @Override
    public void setHumanCharacter(final Character c) {
        this.myModel.humanHasChosenCharacter(c);
        this.turnManager();

    }

    @Override
    public void askQuestion(final Question q) {
        if (this.myModel.askToAI(q)) {
            this.myView.showMessage(q.getText() + " SI");
        } else {
            this.myView.showMessage(q.getText() + " NO");
        }
        this.turnManager();
    }

    @Override
    public void humanAnswered(final Question question, final boolean answer) {
        try {
            this.myModel.humanAnswered(question, answer);
            this.turnManager();

        } catch (UserLiedException e) {
            this.myView.showMessage("Hai mentito!");
            this.myView.showQuestion(question);
        }
    }

    @Override
    public List<String> getPackList() {
        List<String> filenames = new ArrayList<>();
        filenames = ResourceLoader.listResourceFiles(JSONS_PATH).stream()
                .map(x -> x.substring(0, x.length() - JSON_EXT_LENGTH)).collect(Collectors.toList());
        return filenames;
    }

    @Override
    public void tryToGuess(final Character choosen) {
        try {
            this.myModel.humanTryToGuess(choosen);
            if (!this.myModel.humanWon()) {
                this.myView.showMessage("Peccato, il personaggio dell'avversario non e' " + choosen.getName());
            }
        } catch (AttemptException e) {
            this.myView.showMessage("Non hai piu' tentativi!");
        }
        this.turnManager();
    }

    @Override
    public void aiGuessed(final Character c, final boolean answer) {
        try {
            this.myModel.humanAnswered(c, answer);
            this.turnManager();
        } catch (UserLiedException e) {
            this.myView.showMessage("Hai mentito!");
            this.myView.showQuestion(c);
        }

    }

    @Override
    public void loadPack(final String s) {
        this.myModel = new ModelImpl(this.getStartingCharacters(USER_JSON_PATH + "/" + s + ".json"));
        this.myView = new GameViewImpl();
        this.myView.showView();
    }

    @Override
    public void restartGame() {
        this.myModel.resetGame();
    }

    @Override
    public boolean checkLogin(final String id, final String psw) {
        String content = "";
        Scanner c;
        try {
            c = new Scanner(new File(USER_FILE_PATH));
            if (c.hasNext()) {
                content = c.useDelimiter("\\Z").next();
                c.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final String[] accounts = content.split(Pattern.quote(SPLITUSER));
        for (final String a : accounts) {
            final String tmp = a.trim();
            final String[] user = tmp.split(Pattern.quote(SPLITCRED));
            if (user[0].equals(id) && myCrypto.authenticate(psw.toCharArray(), user[1])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean registerUser(final String id, final char... psw) {
        String content = "";
        Scanner c;
        try {
            c = new Scanner(new File(USER_FILE_PATH));
            if (c.hasNext()) {
                content = c.useDelimiter("\\Z").next();
                c.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final String[] accounts = content.split(Pattern.quote(SPLITUSER));
        for (final String a : accounts) {
            final String tmp = a.trim();
            final String[] user = tmp.split(Pattern.quote(SPLITCRED));
            if (user[0].equals(id)) {
                return false;
            }
        }
        return ResourceLoader.writeFile(USER_FILE_PATH, id + SPLITCRED + myCrypto.hash(psw) + SPLITUSER) ? true : false;
    }

    @Override
    public void createCharacter(final String name, final String male, final String eyeColor, final String skinColor,
            final String hairColor, final boolean hasBeard, final boolean hasMust, final String beardColor,
            final String mustacheColor, final boolean hat, final boolean earrings, final boolean glasses,
            final String hairType, final String dressType, final String packages, final String picPath) {
        final CharacterBuilder c = new CharacterBuilder();
        String newPicPath = "";
        final Path tmpVar = Paths.get(picPath).getFileName();
        if (tmpVar != null) {
            newPicPath = USER_IMAGES_PATH + FS + packages + FS + tmpVar.toString();
        }
        this.gson = gsonBuilder.setPrettyPrinting().create();
        if (hasBeard && !hairColor.equals("") && !hasMust) {
            newChar = c.setHairColor(Colors.valueOf(hairColor)).setSkinColor(Colors.valueOf(skinColor))
                    .setEyeColor(Colors.valueOf(eyeColor)).setBeardColor(Colors.valueOf(beardColor))
                    .setMustacheColor(null).setHasHat(hat).setHasEarrings(earrings).setHasGlasses(glasses)
                    .setHairType(Hair.valueOf(hairType)).setDressType(Dress.valueOf(dressType)).setPicPath(newPicPath)
                    .setName(name).setIsMale(male.equals(SEX)).build();

        }
        if (!hasBeard && !hairColor.equals("") && hasMust) {
            newChar = c.setHairColor(Colors.valueOf(hairColor)).setSkinColor(Colors.valueOf(skinColor))
                    .setEyeColor(Colors.valueOf(eyeColor)).setBeardColor(null)
                    .setMustacheColor(Colors.valueOf(mustacheColor)).setHasHat(hat).setHasEarrings(earrings)
                    .setHasGlasses(glasses).setHairType(Hair.valueOf(hairType)).setDressType(Dress.valueOf(dressType))
                    .setPicPath(newPicPath).setName(name).setIsMale(male.equals(SEX)).build();

        }
        if (hasBeard && hairColor.equals("") && !hasMust) {
            newChar = c.setHairColor(null).setSkinColor(Colors.valueOf(skinColor)).setEyeColor(Colors.valueOf(eyeColor))
                    .setBeardColor(Colors.valueOf(beardColor)).setMustacheColor(Colors.valueOf(mustacheColor))
                    .setHasHat(hat).setHasEarrings(earrings).setHasGlasses(glasses).setHairType(Hair.valueOf(hairType))
                    .setDressType(Dress.valueOf(dressType)).setPicPath(newPicPath).setName(name)
                    .setIsMale(male.equals("UOMO")).build();

        }
        if (hasBeard && !hairColor.equals("") && hasMust) {
            newChar = c.setHairColor(Colors.valueOf(hairColor)).setSkinColor(Colors.valueOf(skinColor))
                    .setEyeColor(Colors.valueOf(eyeColor)).setBeardColor(Colors.valueOf(beardColor))
                    .setMustacheColor(Colors.valueOf(mustacheColor)).setHasHat(hat).setHasEarrings(earrings)
                    .setHasGlasses(glasses).setHairType(Hair.valueOf(hairType)).setDressType(Dress.valueOf(dressType))
                    .setPicPath(newPicPath).setName(name).setIsMale(male.equals(SEX)).build();

        }
        if (!hasBeard && hairColor.equals("") && !hasMust) {
            newChar = c.setHairColor(null).setSkinColor(Colors.valueOf(skinColor)).setEyeColor(Colors.valueOf(eyeColor))
                    .setBeardColor(null).setMustacheColor(null).setHasHat(hat).setHasEarrings(earrings)
                    .setHasGlasses(glasses).setHairType(Hair.valueOf(hairType)).setDressType(Dress.valueOf(dressType))
                    .setPicPath(newPicPath).setName(name).setIsMale(male.equals(SEX)).build();
        }
        final Path tmpPath = Paths.get(picPath);
        final Path fileName = tmpPath.getFileName();
        try (FileWriter wr = new FileWriter(USER_JSON_PATH + FS + packages + JSON_EXT, true)) {
            final FileOutputStream s = new FileOutputStream(new File(USER_JSON_PATH + FS + packages + JSON_EXT), true);
            final FileChannel fileChannel = s.getChannel();
            fileChannel.truncate(fileChannel.size() - 2); // Removes last character
            s.close();
            fileChannel.close();
            wr.write(",\n");
            gson.toJson(newChar, wr);
            wr.write("\n]");
            wr.close();
            if (fileName != null) {
                Files.copy(tmpPath, Paths.get(USER_IMAGES_PATH + FS + packages + FS + fileName.toString()),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<String> getColors() {
        return this.myModel.getColors();
    }

    @Override
    public List<String> getHairTypes() {
        return this.myModel.getHairTypes();
    }

    @Override
    public List<String> getDressTypes() {
        return this.myModel.getDressTypes();
    }

    @Override
    public void startApp() {
        new LoginView();
    }

    @Override
    public void callCharView() {
        this.myModel = new ModelImpl();
        new AddCharacterView();

    }

    @Override
    public void startGame() {
        new MenuView();

    }

    /* Utility Methods */

    /**
     * Turn's handler, understand whose turn is and lets him play. Basically the
     * heart of this class.
     */
    private void turnManager() {
        if (!this.checkWin()) {
            if (this.myModel.isHumanTurn()) {
                this.myView.showMessage("E' il tuo turno, puoi porre una domanda oppure provare ad indovinare");

            } else {
                try {
                    final Question q = this.myModel.getAInextQuestion();
                    this.myView.showQuestion(q);
                } catch (NoMoreQuestionAvailableException e) {
                    e.printStackTrace();
                } catch (PendingAIGuessingCharacter e) {
                    this.myView.showQuestion(this.myModel.getAIcharacterGuess().get());
                }
            }
        }
    }

    private boolean checkAndCreate(final String path) {
        final File file = new File(path);
        if (!file.exists() && file.mkdir()) {
            LOG.write("Created " + path.substring(path.lastIndexOf(FS) + 1) + " dir");
            return false;
        }
        return true;
    }

    private boolean checkFile(final String path) {
        final File file = new File(path);
        return file.exists() ? true : false;
    }

    private void moveFiles(final String src, final String dst) {
        List<String> t = new ArrayList<>();
        t = ResourceLoader.listResourceFiles(src);
        for (final String tmp : t) {
            final File f = new File(ResourceLoader.getURL(src + "/" + tmp).getPath());
            try {
                Files.copy(f.toPath(), new File(dst + FS + tmp).toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void moveCharacters(final List<Character> charList, final String src, final String dst) {
        final List<Character> newCharList = new ArrayList<Character>();
        Path tmp;
        /*
         * Moves characters image from resource folder to the local user.home location
         */
        this.moveFiles(src, dst);
        /*
         * Changes the location of the character image for every character contained in
         * the given .json
         */
        for (Character c : charList) {
            final CharacterBuilder cb = new CharacterBuilder(c);
            tmp = Paths.get(c.getPicPath()).getFileName();
            if (tmp != null) {
                cb.setPicPath(dst + FS + tmp.toString());
            }
            c = cb.build();
            newCharList.add(c);
        }
        /* Writes the new .json to the local folder */
        this.gson = this.gsonBuilder.setPrettyPrinting().create();
        tmp = Paths.get(dst).getFileName();
        if (tmp != null) {
            try (FileWriter wr = new FileWriter(USER_JSON_PATH + FS + tmp.toString() + JSON_EXT, true)) {
                gson.toJson(newCharList, wr);
                wr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Checks if someone won the game
     * 
     * @return true if someone won, false otherwise
     */

    private boolean checkWin() {
        if (this.myModel.humanWon()) {
            this.myView.showWinMessage("<html> Hai vinto!<br></br> Il personaggio dell'avversario era: </html>",
                    this.getAIPic());
            return true;
        } else if (this.myModel.aiWon()) {
            this.myView.showWinMessage("<html>Hai perso...<br></br> Il personaggio dell'avversario era: </html>",
                    this.getAIPic());
            return true;
        }
        return false;
    }

    /**
     * 
     * @return the image of AI's choosen character to be shown when someone wins the
     *         game
     */
    private ImageIcon getAIPic() {
        return this.myModel.getAIchosenCharacter().getPic();
    }

    /**
     * 
     * Loads a list of characters from a pack specified by the player.
     * 
     * @return List of characters
     */

    private Set<Character> getStartingCharacters(final String path) {
        final Character[] characters;
        final StringBuilder file = new StringBuilder();
        final String lineFeed = System.getProperty("line.separator");
        try (InputStream in = new FileInputStream(new File(path));
                BufferedReader br = new BufferedReader(new InputStreamReader(in))) {

            br.lines().forEach(x -> file.append(x + lineFeed));
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final String jsonFile = file.toString();
        this.gsonBuilder.registerTypeAdapter(Character.class, new CharacterDeserializer());
        this.gson = gsonBuilder.create();
        characters = gson.fromJson(jsonFile, Character[].class);
        return new LinkedHashSet<>(Arrays.asList(characters));
    }

    private void fixCharactersPic(final String path, final String pack) {
        this.gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Character.class, new CharacterDeserializer());
        this.gson = gsonBuilder.create();
        final Character[] characters = gson.fromJson(ResourceLoader.loadFile(JSONS_PATH + pack + JSON_EXT),
                Character[].class);
        moveCharacters(Arrays.asList(characters), path + pack, USER_IMAGES_PATH + pack);
    }

}
